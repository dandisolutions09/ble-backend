package main

import (
	// "container/list"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	// "time"

	// "time"

	// "time"

	//"github.com/gorilla/handlers"
	// "github.com/gorilla/mux"
	// "go.mongodb.org/mongo-driver/bson"
	// "github.com/gorilla/mux"
	"github.com/patrickmn/go-cache"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "gorm.io/gorm"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	//"go.mongodb.org/mongo-driver/mongo"
	//"go.mongodb.org/mongo-driver/mongo/options"
)

// const ongoing_json = {
// 	distance: postData.distance,
// 	rssi: postData.rssi,
// 	name: postData.deviceAddress,
// 	created_at: new Date().toLocaleString(),
//   };

var dataCache *cache.Cache

type Device struct {
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name      string             `json:"device_name" bson:"device_name"`
	Distance  string             `json:"distance" bson:"distance"`
	Rssi      string             `json:"rssi" bson:"rssi"`
	Bridge    string             `json:"bridge" bson:"bridge"`
	CreatedAt string             `json:"created_at" bson:"created_at"`
}

// { id: 0, value: 10, label: "series A" },
// { id: 1, value: 15, label: "series B" },
// { id: 2, value: 20, label: "series C" },

type GraphData struct {
	TAG_1_GreenValue int    `json:"tag_1_green_value" bson:"tag_1_green_value"`
	TAG_1_GreenLabel string `json:"tag_1_green_label" bson:"tag_1_green_label"`

	TAG_1_RedValue int    `json:"tag_1_red_value" bson:"tag_1_red_value"`
	TAG_1_RedLabel string `json:"tag_1_red_label" bson:"tag_1_red_label"`

	TAG_2_GreenValue int    `json:"tag_2_green_value" bson:"tag_2_green_value"`
	TAG_2_GreenLabel string `json:"tag_2_green_label" bson:"tag_2_green_label"`

	TAG_2_RedValue int    `json:"tag_2_red_value" bson:"tag_2_red_value"`
	TAG_2_RedLabel string `json:"tag_2_red_label" bson:"tag_2_red_label"`

	TAG_3_GreenValue int    `json:"tag_3_green_value" bson:"tag_3_green_value"`
	TAG_3_GreenLabel string `json:"tag_3_green_label" bson:"tag_3_green_label"`

	TAG_3_RedValue int    `json:"tag_3_red_value" bson:"tag_3_red_value"`
	TAG_3_RedLabel string `json:"tag_3_red_label" bson:"tag_3_red_label"`

	TAG_4_GreenValue int    `json:"tag_4_green_value" bson:"tag_4_green_value"`
	TAG_4_GreenLabel string `json:"tag_4_green_label" bson:"tag_4_green_label"`

	TAG_4_RedValue int    `json:"tag_4_red_value" bson:"tag_4_red_value"`
	TAG_4_RedLabel string `json:"tag_4_red_label" bson:"tag_4_red_label"`

	// TAG_3_GreenLabel string `json:"tag_3_green_label" bson:"tag_3_green_label"`
	// TAG_3_RedLabel   string `json:"tag_3_red_label" bson:"tag_3_red_label"`

	// TAG_4_Value      int    `json:"tag_4_value" bson:"tag_4_value"`
	// TAG_4_GreenLabel string `json:"tag_4_green_label" bson:"tag_4_green_label"`
	// TAG_4_RedLabel   string `json:"tag_4_red_label" bson:"tag_4_red_label"`
}

func handleAddDeviceInfo(w http.ResponseWriter, r *http.Request) {
	// Parse JSON data from the request
	fmt.Println("incoming data:", r.Body)

	decoder := json.NewDecoder(r.Body)
	var data Device
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	fmt.Println("data", data)

	// Insert data into MongoDB
	collection := client.Database(dbName).Collection(devices_colletion)
	_, err = collection.InsertOne(context.Background(), data)
	fmt.Print(data)
	if err != nil {
		http.Error(w, "Error inserting nurse", http.StatusInternalServerError)
		return
	}

	//	Respond with a success message
	fmt.Fprintf(w, "Device inserted Successfully")

}

//GET FACILITY BY ID

//GET ALL FACILITIES

func handleGetDevices(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get devices!!!----")

	if cachedData, found := dataCache.Get("ble-data-cache"); found {
		// Data found in cache, return it
		fmt.Println("Data found in cache!")
		w.Header().Set("Content-Type", "application/json")
		w.Write(cachedData.([]byte))
		return
	}
	collection := client.Database(dbName).Collection(devices_colletion)

	var devices []Device

	var tag_1_red_devices []Device
	var tag_1_green_devices []Device

	var tag_2_red_devices []Device
	var tag_2_green_devices []Device

	var tag_3_red_devices []Device
	var tag_3_green_devices []Device

	var tag_4_red_devices []Device
	var tag_4_green_devices []Device

	var tag_5_red_devices []Device
	var tag_5_green_devices []Device

	//var graphData []GraphData

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		http.Error(w, "Error retrieving devices", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var device Device
		if err := cursor.Decode(&device); err != nil {
			http.Error(w, "Error decoding device", http.StatusInternalServerError)
			return
		}
		if device.Name != "" && device.Name != "NaN" {

			//fmt.Println("TAG-", device.Name)

			if device.Name == "TAG-1:" {
				//fmt.Println("TAG-1", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_1_red_devices = append(tag_1_red_devices, device)
					} else if rssi < -50 {
						tag_1_green_devices = append(tag_1_green_devices, device)
					}
				}

			} else if device.Name == "TAG-2:" {
				//fmt.Println("TAG-2", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_2_red_devices = append(tag_2_red_devices, device)
					} else if rssi < -50 {
						tag_2_green_devices = append(tag_2_green_devices, device)
					}
				}

			} else if device.Name == "TAG-3:" {
				//fmt.Println("TAG-3", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_3_red_devices = append(tag_3_red_devices, device)
					} else if rssi < -50 {
						tag_3_green_devices = append(tag_3_green_devices, device)
					}
				}

			} else if device.Name == "TAG-4:" {
				//fmt.Println("TAG-4", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_4_red_devices = append(tag_4_red_devices, device)
					} else if rssi < -50 {
						tag_4_green_devices = append(tag_4_green_devices, device)
					}
				}

			} else if device.Name == "TAG-5:" {
				//fmt.Println("TAG-5", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_5_red_devices = append(tag_5_red_devices, device)
					} else if rssi < -50 {
						tag_5_green_devices = append(tag_5_green_devices, device)
					}
				}

			}

		}

		devices = append(devices, device)

		// Get the length of the slices

	}

	tag_1_greenLength := len(tag_1_green_devices)
	tag_1_redLength := len(tag_1_red_devices)

	tag_2_greenLength := len(tag_2_red_devices)
	tag_2_redLength := len(tag_2_green_devices)

	tag_3_greenLength := len(tag_3_red_devices)
	tag_3_redLength := len(tag_3_green_devices)

	tag_4_greenLength := len(tag_4_red_devices)
	tag_4_redLength := len(tag_4_green_devices)

	tag_5_greenLength := len(tag_5_red_devices)
	tag_5_redLength := len(tag_5_green_devices)

	fmt.Println("tag 1 Green Length:-", tag_1_greenLength)
	fmt.Println("tag 1 Red Length:->", tag_1_redLength)

	fmt.Println("tag 2 Green Length:-", tag_2_greenLength)
	fmt.Println("tag 2 Red Length:->", tag_2_redLength)

	fmt.Println("tag 3 Green Length:-", tag_3_greenLength)
	fmt.Println("tag 3 Red Length:->", tag_3_redLength)

	fmt.Println("tag 4 Green Length:-", tag_4_greenLength)
	fmt.Println("tag 4 Red Length:->", tag_4_redLength)

	fmt.Println("tag 5 Green Length:-", tag_5_greenLength)
	fmt.Println("tag 5 Red Length:->", tag_5_redLength)

	// Convert partners slice to JSON
	jsonData, err := json.Marshal(devices)
	if err != nil {
		http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
		return
	}

	dataCache.Set("ble-data-cache", jsonData, cache.DefaultExpiration)

	// Respond with the retrieved facilities in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(devices)
}

//GET GRAPH DATA

func handleGetGraphs(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get devices!!!----")
	collection := client.Database(dbName).Collection(devices_colletion)

	// Define a slice to store retrieved facilities
	//var devices_green []Device

	//var devices_red []Device

	//var tag_1_devices []Device
	var tag_1_red_devices []Device
	var tag_1_green_devices []Device

	var tag_2_red_devices []Device
	var tag_2_green_devices []Device

	var tag_3_red_devices []Device
	var tag_3_green_devices []Device

	var tag_4_red_devices []Device
	var tag_4_green_devices []Device

	var tag_5_red_devices []Device
	var tag_5_green_devices []Device

	//var graphData []GraphData

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		http.Error(w, "Error retrieving devices", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var device Device
		if err := cursor.Decode(&device); err != nil {
			http.Error(w, "Error decoding device", http.StatusInternalServerError)
			return
		}
		if device.Name != "" && device.Name != "NaN" {

			//fmt.Println("TAG-", device.Name)

			if device.Name == "TAG-1:" {
				//fmt.Println("TAG-1", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_1_red_devices = append(tag_1_red_devices, device)
					} else if rssi < -50 {
						tag_1_green_devices = append(tag_1_green_devices, device)
					}
				}

			} else if device.Name == "TAG-2:" {
				//fmt.Println("TAG-2", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_2_red_devices = append(tag_2_red_devices, device)
					} else if rssi < -50 {
						tag_2_green_devices = append(tag_2_green_devices, device)
					}
				}

			} else if device.Name == "TAG-3:" {
				//fmt.Println("TAG-3", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_3_red_devices = append(tag_3_red_devices, device)
					} else if rssi < -50 {
						tag_3_green_devices = append(tag_3_green_devices, device)
					}
				}

			} else if device.Name == "TAG-4:" {
				//fmt.Println("TAG-4", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_4_red_devices = append(tag_4_red_devices, device)
					} else if rssi < -50 {
						tag_4_green_devices = append(tag_4_green_devices, device)
					}
				}

			} else if device.Name == "TAG-5:" {
				//fmt.Println("TAG-5", device)
				//tag_1_devices = append(tag_1_devices, device)

				// Check if RSSI value is not empty or "NaN"
				if device.Rssi != "" && device.Rssi != "NaN" {
					// Convert RSSI string to integer
					rssi, err := strconv.Atoi(device.Rssi)
					if err != nil {
						fmt.Println("ERR", err)
						http.Error(w, "Error converting RSSI to integer", http.StatusInternalServerError)
						return
					}

					// Filter devices based on RSSI values
					if rssi <= -39 && rssi >= -50 {
						tag_5_red_devices = append(tag_5_red_devices, device)
					} else if rssi < -50 {
						tag_5_green_devices = append(tag_5_green_devices, device)
					}
				}

			}

		}

		//devices = append(devices, device)

		// Get the length of the slices

	}

	tag_1_greenLength := len(tag_1_green_devices)
	tag_1_redLength := len(tag_1_red_devices)

	tag_2_greenLength := len(tag_2_red_devices)
	tag_2_redLength := len(tag_2_green_devices)

	tag_3_greenLength := len(tag_3_red_devices)
	tag_3_redLength := len(tag_3_green_devices)

	tag_4_greenLength := len(tag_4_red_devices)
	tag_4_redLength := len(tag_4_green_devices)

	tag_5_greenLength := len(tag_5_red_devices)
	tag_5_redLength := len(tag_5_green_devices)

	fmt.Println("tag 1 Green Length:-", tag_1_greenLength)
	fmt.Println("tag 1 Red Length:->", tag_1_redLength)

	fmt.Println("tag 2 Green Length:-", tag_2_greenLength)
	fmt.Println("tag 2 Red Length:->", tag_2_redLength)

	fmt.Println("tag 3 Green Length:-", tag_3_greenLength)
	fmt.Println("tag 3 Red Length:->", tag_3_redLength)

	fmt.Println("tag 4 Green Length:-", tag_4_greenLength)
	fmt.Println("tag 4 Red Length:->", tag_4_redLength)

	fmt.Println("tag 5 Green Length:-", tag_5_greenLength)
	fmt.Println("tag 5 Red Length:->", tag_5_redLength)

	// TAG_1_Value      int    `json:"tag_1_value" bson:"tag_1_value"`
	// TAG_1_GreenLabel string `json:"tag_1_green_label" bson:"tag_1_green_label"`
	// TAG_1_RedLabel   string `json:"tag_1_red_label" bson:"tag_1_red_label"`

	// TAG_2_Value      int    `json:"tag_2_value" bson:"tag_2_value"`
	// TAG_2_GreenLabel string `json:"tag_2_green_label" bson:"tag_2_green_label"`
	// TAG_2_RedLabel   string `json:"tag_2_red_label" bson:"tag_2_red_label"`

	// TAG_3_Value      int    `json:"tag_3_value" bson:"tag_3_value"`
	// TAG_3_GreenLabel string `json:"tag_3_green_label" bson:"tag_3_green_label"`
	// TAG_3_RedLabel   string `json:"tag_3_red_label" bson:"tag_3_red_label"`

	// TAG_4_Value      int    `json:"tag_4_value" bson:"tag_4_value"`
	// TAG_4_GreenLabel string `json:"tag_4_green_label" bson:"tag_4_green_label"`
	// TAG_4_RedLabel   string `json:"tag_4_red_label" bson:"tag_4_red_label"`

	Graph := GraphData{
		TAG_1_GreenValue: tag_1_greenLength,
		TAG_1_GreenLabel: "tag 1 GREEN",

		TAG_1_RedValue: tag_1_redLength,
		TAG_1_RedLabel: "tag 1 RED",

		///////////////////////////////////////////////////////////

		TAG_2_GreenValue: tag_2_greenLength,
		TAG_2_GreenLabel: "TAG 2 GREEN",

		TAG_2_RedValue: tag_2_redLength,
		TAG_2_RedLabel: "TAG 2 RED",
		///////////////////////////////////////////////////////////

		TAG_3_GreenValue: tag_3_greenLength,
		TAG_3_GreenLabel: "TAG 3 GREEN",

		TAG_3_RedValue: tag_3_redLength,
		TAG_3_RedLabel: "TAG 3 RED",

		///////////////////////////////////////////////////////////

		TAG_4_GreenValue: tag_4_greenLength,
		TAG_4_GreenLabel: "TAG 4 GREEN",

		TAG_4_RedValue: tag_4_redLength,
		TAG_4_RedLabel: "TAG 4 RED",

		///////////////////////////////////////////////////////////

	}

	//for x in 7

	// for i := 0; i <= 6; i++ {
	// 	fmt.Println(i)

	// 	// type GraphData struct {
	// 	// 	Value      string `json:"value" bson:"value"`
	// 	// 	GreenLabel string `json:"green_label" bson:"green_label"`
	// 	// 	RedLabel   string `json:"red_label" bson:"red_label"`
	// 	// 	CreatedAt  string `json:"created_at" bson:"created_at"`
	// 	// }

	// }

	// Tag_1_Graph_Green := GraphData{
	// 	Value:      tag_1_greenLength,
	// 	GreenLabel: "tag 1 greenValue",
	// }

	// graphData = append(graphData, Tag_1_Graph_Green)

	// Tag_1_Graph_Red := GraphData{
	// 	Value:    tag_1_redLength,
	// 	RedLabel: "tag 1 RedValue",
	// }
	// graphData = append(graphData, Tag_1_Graph_Red)

	// ////////////////////////////////////////////////////////////////////////////

	// Tag_2_Graph_Green := GraphData{
	// 	Value:      tag_2_greenLength,
	// 	GreenLabel: "tag 2 greenValue",
	// }

	// graphData = append(graphData, Tag_2_Graph_Green)

	// Tag_2_Graph_Red := GraphData{
	// 	Value:    tag_2_redLength,
	// 	RedLabel: "tag 2 redValue",
	// }

	// graphData = append(graphData, Tag_2_Graph_Red)
	// /////////////////////////////////////////////////////////////////////////////////

	// Tag_3_Graph_Green := GraphData{
	// 	Value:      tag_3_greenLength,
	// 	GreenLabel: "tag 3 greenValue",
	// }

	// graphData = append(graphData, Tag_3_Graph_Green)

	// Tag_3_Graph_Red := GraphData{
	// 	Value:    tag_3_redLength,
	// 	RedLabel: "tag_3_redLength",
	// }

	// graphData = append(graphData, Tag_3_Graph_Red)

	// /////////////////////////////////////////////////////////////////////////////////

	// Tag_4_Graph_Green := GraphData{
	// 	Value:      tag_4_greenLength,
	// 	GreenLabel: "tag 4 greenValue",
	// }

	// graphData = append(graphData, Tag_4_Graph_Green)

	// Tag_4_Graph_Red := GraphData{
	// 	Value:    tag_4_greenLength,
	// 	RedLabel: "tag 4 redValue",
	// }

	// graphData = append(graphData, Tag_4_Graph_Red)

	// /////////////////////////////////////////////////////////////////////////////////

	// Tag_5_Graph_Green := GraphData{
	// 	Value:      tag_5_greenLength,
	// 	GreenLabel: "tag 5 greenValue",
	// }

	// graphData = append(graphData, Tag_5_Graph_Green)

	// Tag_5_Graph_Red := GraphData{
	// 	Value:    tag_5_redLength,
	// 	RedLabel: "tag 5 RedValue",
	// }

	//graphData = append(graphData, Tag_5_Graph_Red)

	/////////////////////////////////////////////////////////////////////////////////

	// Respond with the retrieved facilities in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(Graph)
}

// func filterCustomersByStatus(w http.ResponseWriter, r *http.Request) {
// 	status := r.URL.Query().Get("status")

// 	fmt.Println("Status", status)

// 	// Check if subcategory is "all"
// 	if status == "ALL" {
// 		// Fetch all services from MongoDB
// 		var devices []Device
// 		collection := client.Database(dbName).Collection(devices_colletion)
// 		cursor, err := collection.Find(context.Background(), bson.D{})
// 		if err != nil {
// 			http.Error(w, err.Error(), http.StatusInternalServerError)
// 			return
// 		}
// 		defer cursor.Close(context.Background())
// 		for cursor.Next(context.Background()) {
// 			var device Device
// 			err := cursor.Decode(&device)
// 			if err != nil {
// 				http.Error(w, err.Error(), http.StatusInternalServerError)
// 				return
// 			}
// 			devices = append(devices, device)
// 		}

// 		// Respond with the retrieved services
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(devices)
// 		return
// 	}

// 	// Fetch partners from MongoDB based on subcategory
// 	var customers []Device
// 	collection := client.Database(dbName).Collection(devices_colletion)
// 	cursor, err := collection.Find(context.Background(), bson.M{"status": status})
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusInternalServerError)
// 		return
// 	}
// 	defer cursor.Close(context.Background())
// 	for cursor.Next(context.Background()) {
// 		var customer Customer
// 		err := cursor.Decode(&customer)
// 		if err != nil {
// 			http.Error(w, err.Error(), http.StatusInternalServerError)
// 			return
// 		}
// 		customers = append(customers, customer)
// 	}

// 	// Respond with the retrieved services
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(customers)
// }

func filterCustomersByTime(w http.ResponseWriter, r *http.Request) {
	status := r.URL.Query().Get("status")

	fmt.Println("Status", status)

	var filter bson.D

	// Check status value and set filter accordingly
	switch status {
	case "LAST_HOUR":
		// Calculate the time one hour ago
		layout := "1/2/2006, 3:04:05 PM"
		oneHourAgo := time.Now().Add(-time.Hour)
		oneHourAgoStr := oneHourAgo.Format(layout)

		fmt.Println("one hour ago", oneHourAgo)
		filter = bson.D{{"created_at", bson.D{{"$gte", oneHourAgoStr}}}}

		fmt.Println("canges")
	case "LAST_24_HOURS":
		// Calculate the time 24 hours ago
		layout := "1/2/2006, 3:04:05 PM"
		twentyFourHoursAgo := time.Now().Add(-24 * time.Hour)
		twentyFourHoursAgoStr := twentyFourHoursAgo.Format(layout)
		// twentyFourHoursAgo := time.Now().Add(-24 * time.Hour)
		fmt.Println("24hours ", twentyFourHoursAgoStr)
		filter = bson.D{{"created_at", bson.D{{"$gte", twentyFourHoursAgoStr}}}}
	case "LAST_WEEK":
		// Calculate the start time for exactly one week ago
		//oneWeekAgo := time.Now().AddDate(0, 0, -7)

		layout := "1/2/2006, 3:04:05 PM"
		oneWeekAgo := time.Now().AddDate(0, 0, -7)
		oneWeekAgoStr := oneWeekAgo.Format(layout)
		// twentyFourHoursAgo := time.Now().Add(-24 * time.Hour)
		fmt.Println("1 week ago ", oneWeekAgoStr)
		filter = bson.D{{"created_at", bson.D{{"$gt", oneWeekAgoStr}}}}
	case "LAST_MONTH":
		// Calculate the start time for the beginning of the current month
		now := time.Now()
		startOfMonth := time.Date(now.Year(), now.Month(), 1, 0, 0, 0, 0, now.Location())
		filter = bson.D{{"created_at", bson.D{{"$gte", startOfMonth.Format("1/2/2006, 03:04:05 PM")}}}}

	case "LAST_3_MONTHS":
		// Calculate the start time for exactly three months ago
		threeMonthsAgo := time.Now().AddDate(0, -3, 0)
		filter = bson.D{{"created_at", bson.D{{"$gte", threeMonthsAgo.Format("1/2/2006, 03:04:05 PM")}}}}
	default:
		http.Error(w, "Invalid status value", http.StatusBadRequest)
		return
	}

	var devices []Device

	collection := client.Database(dbName).Collection(devices_colletion)
	cursor, err := collection.Find(context.Background(), filter)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	for cursor.Next(context.Background()) {
		var device Device
		err := cursor.Decode(&device)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		devices = append(devices, device)
	}

	// Respond with the retrieved devices
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(devices)
}

// func filterCustomersByTime(w http.ResponseWriter, r *http.Request) {
// 	status := r.URL.Query().Get("status")

// 	fmt.Println("Status", status)

// 	var filter bson.D

// 	// Calculate the start time for the beginning of the current time
// 	now := time.Now()

// 	// Check status value and set filter accordingly
// 	switch status {
// 	case "LAST_HOUR":
// 		// Calculate the time one hour ago
// 		oneHourAgo := now.Add(-time.Hour)
// 		fmt.Println("ONE HOUR", oneHourAgo)
// 		filter = bson.D{{"created_at", bson.D{{"$gte", oneHourAgo.Format(time.RFC3339)}}}}
// 	case "LAST_24_HOURS":
// 		// Calculate the time 24 hours ago
// 		twentyFourHoursAgo := now.Add(-24 * time.Hour)
// 		filter = bson.D{{"created_at", bson.D{{"$gte", twentyFourHoursAgo.Format(time.RFC3339)}}}}
// 	case "LAST_WEEK":
// 		// Set the filter to start from the beginning of the current week
// 		// Note: This block is removed as per your request
// 	case "LAST_MONTH":
// 		// Set the filter to start from the beginning of the current month
// 		startOfMonth := time.Date(now.Year(), now.Month(), 1, 0, 0, 0, 0, now.Location())
// 		filter = bson.D{{"created_at", bson.D{{"$gte", startOfMonth.Format(time.RFC3339)}}}}
// 	case "LAST_3_MONTHS":
// 		// Set the filter to start from the beginning of the current month minus 2 months
// 		startOfMonth := time.Date(now.Year(), now.Month(), 1, 0, 0, 0, 0, now.Location())
// 		threeMonthsAgo := startOfMonth.AddDate(0, -2, 0)
// 		filter = bson.D{{"created_at", bson.D{{"$gte", threeMonthsAgo.Format(time.RFC3339)}}}}
// 	default:
// 		http.Error(w, "Invalid status value", http.StatusBadRequest)
// 		return
// 	}

// 	var devices []Device

// 	collection := client.Database(dbName).Collection(devices_colletion)
// 	cursor, err := collection.Find(context.Background(), filter)
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusInternalServerError)
// 		return
// 	}
// 	defer cursor.Close(context.Background())

// 	for cursor.Next(context.Background()) {
// 		var device Device
// 		err := cursor.Decode(&device)
// 		if err != nil {
// 			http.Error(w, err.Error(), http.StatusInternalServerError)
// 			return
// 		}
// 		devices = append(devices, device)
// 	}

// 	// Respond with the retrieved devices
// 	w.Header().Set("Content-Type", "application/json")
// 	err = json.NewEncoder(w).Encode(devices)
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusInternalServerError)
// 		return
// 	}
// }

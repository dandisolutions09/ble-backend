package main

import (
	"context"
	"time"

	"fmt"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/patrickmn/go-cache"

	"gorm.io/gorm"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gorm.io/datatypes"
	// "gorm.io/driver/sqlite"
	// "gorm.io/gorm"
	// "log"
)

// package level variable
var dbName = "rssidata"

var devices_colletion = "devices_colletion"

// var settings_collection = "settings_collection"

var mongoURI = "mongodb+srv://oluwashayomi:eniola1990@cluster0.kcef1.mongodb.net/"

var client *mongo.Client

type NurseData struct {
	gorm.Model
	Name               string         `json:"name"`
	Department         string         `json:"department"`
	Wards              datatypes.JSON `json:"wards"`
	Grade              string         `json:"grade"`
	SignatureImageData string         `json:"signatureImageData"`
}

var partnersCache *cache.Cache

func init() {

	var err error

	// Set up MongoDB client
	clientOptions := options.Client().ApplyURI(mongoURI)

	client, err = mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		fmt.Println("Error connecting to MongoDB:", err)
		return
	}

	// Check the connection
	err = client.Ping(context.Background(), nil)
	if err != nil {
		fmt.Println("Error pinging MongoDB:", err)
		return
	}

	fmt.Println("Connected to MongoDB")

	dataCache = cache.New(20*time.Minute, 20*time.Minute)
	//dataCache := cache.New(20*time.Minute, 20*time.Minute)

}

// Define a struct for your data (adjust fields as needed)
func main() {

	router := mux.NewRouter()

	// Use the handlers.CORS middleware to handle CORS
	corsHandler := handlers.CORS(
		handlers.AllowedHeaders([]string{"Content-Type", "Authorization"}),
		handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"}),
		handlers.AllowedOrigins([]string{"*"}),
	)

	// Attach the CORS middleware to your router

	//FACILITY ROUTES

	router.HandleFunc("/add-logs", handleAddDeviceInfo).Methods("POST")
	router.HandleFunc("/get-data", handleGetDevices).Methods("GET")
	router.HandleFunc("/get-graphs", handleGetGraphs).Methods("GET")

	router.HandleFunc("/filter-data", filterCustomersByTime).Methods("GET")

	//filterCustomersByTime

	// Start the server on port 8080
	http.Handle("/", corsHandler(router))
	fmt.Println("Server listening on :8080")
	http.ListenAndServe(":8080", nil)
}
